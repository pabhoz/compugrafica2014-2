var scene,camera,renderer;
var ultiTiempo;
var labels = [];
var objetos = [];
var appW = window.innerWidth;
var appH = window.innerHeight;

function webGLStart(){
	iniciarEscena();
	ultiTiempo = Date.now();
	animarEscena();
}

function iniciarEscena(){
	//renderer = new THREE.WebGLRenderer( {antialias: false} );
	renderer = new THREE.WebGLRenderer( {antialias: true} );
	renderer.setSize(appW, appH);
	document.body.appendChild(renderer.domElement);

	var view_angle = 45, aspect_ratio = appW/appH, near = 0.1, far = 20000;
	//var view_angle = 45, aspect_ratio = appW/appH, near = 0.1, far = 100;
	camera = new THREE.PerspectiveCamera(view_angle, aspect_ratio, near, far);
	camera.position.set(0,500,500);
	camera.lookAt(new THREE.Vector3(0,0,0));

	scene = new THREE.Scene();

	//Iniciar controles de la camara
	controlCamara = new THREE.OrbitControls( camera , renderer.domElement );

	//Stats
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '10px';
	stats.domElement.style.zIndex = '100';
	document.body.appendChild( stats.domElement );

	//Triangulo
	/*Material -> texturas
	Geometria
	Objeto3D -> (Material y Geometria)*/
	var materialTriangulo = new THREE.MeshBasicMaterial({
		vertexColors: THREE.VertexColors
	});

	var geometriaTriangulo = new THREE.Geometry();

	geometriaTriangulo.vertices.push(new THREE.Vector3(  0.0, 100.0,  -100.0) );
	geometriaTriangulo.vertices.push(new THREE.Vector3( -100.0, -100.0,  0.0) );
	geometriaTriangulo.vertices.push(new THREE.Vector3(  100.0, -100.0,  0.0) );
	geometriaTriangulo.vertices.push(new THREE.Vector3(  100.0, -100.0,  -200.0) );
	geometriaTriangulo.faces.push(new THREE.Face3(0,1,2));
	geometriaTriangulo.faces[0].vertexColors[0] = new THREE.Color(0xFF0000);
	geometriaTriangulo.faces[0].vertexColors[1] = new THREE.Color(0x00FF00);
	geometriaTriangulo.faces[0].vertexColors[2] = new THREE.Color(0x0000FF);

	geometriaTriangulo.faces.push(new THREE.Face3(0,2,3));
	geometriaTriangulo.faces[1].vertexColors[0] = new THREE.Color(0xFF0000);
	geometriaTriangulo.faces[1].vertexColors[2] = new THREE.Color(0x0000FF);
	geometriaTriangulo.faces[1].vertexColors[3] = new THREE.Color(0x00FFFF);

	var triangulo = new THREE.Mesh( geometriaTriangulo , materialTriangulo);
	triangulo.position.set(0.0,0.0,-7.0);
	scene.add(triangulo);
	console.log(triangulo);
}

function animarEscena(){
	requestAnimationFrame( animarEscena );
	renderEscena();
	actualizarEscena();
}

function renderEscena(){
	renderer.render( scene, camera );
}

function actualizarEscena(){
	var delta = (Date.now() - ultiTiempo)/1000;
	ultiTiempo = Date.now();

	for (var i = 0; i < labels.length; i++) {
		labels[i].lookAt(camera.position);
	};

	controlCamara.update();
	stats.update();

}